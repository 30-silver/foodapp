from django.shortcuts import redirect
from django.http import HttpResponse
from django.template import loader

from .models import FoodModel
from .forms import FoodForm

# Create your views here.

def index(request):
    form = FoodForm(request.POST or None)

    if form.is_valid():
        form.save()
        return redirect("/")
    
    response = loader.get_template("index.html")
    context = {
        "foods": FoodModel.objects.all(),
        "food_form": form
    }

    return HttpResponse(response.render(context, request))

def details(request, food_name):
    response = loader.get_template("details.html")
    context = {
        "food": FoodModel.objects.get(name=food_name)
    }

    return HttpResponse(response.render(context, request))