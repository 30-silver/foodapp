from django.db import models

# Create your models here.
class FoodModel(models.Model):
    def __str__(self):
        return self.name + self.desc + self.imgurl 

    name = models.CharField(max_length=100, null=False, unique=True)
    desc = models.CharField(max_length=512, null=False)
    imgurl = models.CharField(max_length=512, null=False, default='https://www.russorizio.com/wp-content/uploads/2016/07/ef3-placeholder-image.jpg')