from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name="index"),
    path('food/<str:food_name>', views.details, name="details")
]